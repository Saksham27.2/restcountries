import React, { useState, useEffect, useContext } from 'react';
import '../App.css'
import Filter from './Filter';
import Loader from './Loader';
import { useHandle } from './GlobalContext/GlobalContext';
import { Link } from 'react-router-dom';




const Allcountries = () => {
    const {
        countries,
        filteredcountry,
        setcountries,
        input,
        setinput,
        error,
        setfilteredcountry,
        setError,
        populationFilter,
        setpopulationFilter, areafilter,
        setareaFilter,
        darktheme,
        setdarktheme,
        selectedRegion,
        setSelectedRegion,
        SelectedSubRegion

    } = useHandle() // Use useContext to access the context

    const [isloading, setisloading] = useState(true)
    const getallcountries = async () => {
        try {
            const response = await fetch('https://restcountries.com/v3.1/all')

            if (!response.ok) {
                throw new Error('Something went wrong');
            }

            const data = await response.json();
            console.log(data)
            setcountries(data);

            setfilteredcountry(data);
            setisloading(false);
        }
        catch (err) {
            setisloading(false);
            setError(err.message);
        }
    }
    const getCountryByName = (countryName) => {
        
        let filtered = countries.filter(data => data.name.common.trim().toLowerCase().includes(countryName.trim().toLowerCase()));
        // console.log(filtered);
        if (filtered.length === 0) {
            setfilteredcountry()
        }
        setfilteredcountry(filtered);
    };

    const handlepopulationFilter = (e) => {
        const filter = e.target.value;
        console.log(filter);
        setpopulationFilter(filter);


        let sortedcountries = [...filteredcountry];
        // console.log(sortedcountries, 'Hello')

        if (filter === 'Increasing') {
            sortedcountries.sort((a, b) => parseInt(a.population) - parseInt(b.population));
        } else if (filter === 'Decreasing') {
            sortedcountries.sort((a, b) => parseInt(b.population) - parseInt(a.population));
        }

        setfilteredcountry(sortedcountries);
    };
    const handleareaFilter = (e) => {
        const filter = e.target.value;
        console.log(filter);
        setareaFilter(filter)


        let sortedcountries = [...filteredcountry];
        console.log(sortedcountries, 'Hello')

        if (filter === 'Increasing') {
            sortedcountries.sort((a, b) => (a.area) - (b.area));
        }
        else if (filter === 'Decreasing') {
            sortedcountries.sort((a, b) => (b.area) - (a.area));
        }

        setfilteredcountry(sortedcountries);
    };
    const getCountryByRegion = (region) => {
        setSelectedRegion(region)
        // let filtered = countries.filter(data => data.region.toLowerCase().includes(region.toLowerCase()));

        console.log(region, typeof region, selectedRegion, SelectedSubRegion, typeof selectedRegion, typeof SelectedSubRegion)
        let filtered= countries.filter((country) => {
            return (country.region.toLowerCase().includes(region.toLowerCase()) && country.subregion.toLowerCase().includes(SelectedSubRegion.toLowerCase()))
        })
        
        setfilteredcountry(filtered);
    };
    const subregionobject = {}
    const getCountryBysubRegion = () => {
        // console.log(countries, 'Hello');
        countries.forEach(element => {
            if ((!subregionobject[element.region])) {
                subregionobject[element.region] = []
            }
            if (!subregionobject[element.region].includes(element.subregion)) {
                subregionobject[element.region].push(element.subregion)
            }

        });
    }

    getCountryBysubRegion()
    // getCountryByRegion

    
    console.log(SelectedSubRegion);


    const darkstyle = {
        backgroundColor: 'hsl(207, 26%, 17%)',
        color: 'hsl(0, 0%, 100%)',

    }
    const lightstyle = {
        backgroundColor: ' hsl(0, 0%, 98%)',
        color: '#000'
    }
    // const darkstyle1 = {
    //     backgroundColor: 'hsl(209, 23%, 22%)',
    //     color: 'hsl(0, 0%, 100%)'
    // }
    // const lightstyle1 = {
    //     backgroundColor: ' hsl(0, 0%, 98%)',
    //     color: '#000'
    // }



    useEffect(() => {
        getallcountries();
    }, [])

    useEffect(()=> {
        getCountryByRegion(selectedRegion)
        getCountryBysubRegion()
    }, [SelectedSubRegion, selectedRegion, input])




    return (

        <div className='all_country' style={!darktheme ? darkstyle : lightstyle}>
            <div className="search_country">
            
                <Filter onSearch={getCountryByName} onfilter={getCountryByRegion} handlepopulationFilter={handlepopulationFilter} populationFilter={populationFilter} handleareaFilter={handleareaFilter} areafilter={areafilter} subregion={subregionobject}  />

            </div>
            <div className="allbottom">
                {isloading && !error && <Loader />}
                {/* {!isloading && error && <h4>{error}</h4>} */}
                {!isloading && filteredcountry.length === 0 && <h3>No country found!</h3>}



                {
                    filteredcountry &&
                    filteredcountry.map((country,index) => (

                        <Link to={`/country/${country.name.common}`} className={!darktheme ? 'dark-theme ah5' : 'light-theme ah5'}>
                        <div className={!darktheme ? 'dark-theme country_card' : 'light-theme country_card'} key={index}  >

                            <div className="image">
                                <img src={country.flags.png} alt='' />
                            </div>
                            <div className="country_info">

                                <h3 className={!darktheme ? 'dark-themea countryname' : 'light-themea countryname'}>{country.name.common}</h3>
                                <h5 >
                                    {" "}
                                    Population:{" "}
                                    <span className='h5span'>{new Intl.NumberFormat().format(country.population)}</span>
                                </h5>
                                <h5>Region : <span className='h5span'>{country.region}</span></h5>
                                <h5>Capital:<span className='h5span'> {country.capital}</span></h5>
                                {/* <h5>Area :  {country.area}</h5> */}
                            </div>
                        </div>
                        </Link>  
                    ))
                }


            </div>
        </div>

    )
}
export default Allcountries

