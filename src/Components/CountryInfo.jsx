import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { Link } from 'react-router-dom'
import Loader from './Loader'
import '../country.css'
import { useHandle } from './GlobalContext/GlobalContext'


const CountryInfo = () => {
  const {
    darktheme
  }=useHandle()

  let api = 'https://restcountries.com/v3.1'
  const [country, setcountry] = useState([]);
  const [isloading, setisloading] = useState(true)
  const [error, seterror] = useState('');

  const { countryName } = useParams()

  useEffect(() => {
    const getCountryByName = async () => {
      try {
        let response = await fetch(`${api}/name/${countryName}`)

        if (!response.ok) {
          throw new Error('No Country Found !');
        }
        const data = await response.json();

        setcountry(data);
        setisloading(false);

      }
      catch (err) {
        seterror(error.message);
        setisloading(false);
      }
    }
    getCountryByName()
  }, [countryName])

  const darkstyle = {
    backgroundColor: 'hsl(207, 26%, 17%)',
    color: 'hsl(0, 0%, 100%)',
    marginTop: '5rem',
    width:'100vw',
    height:'91.4vh'
  }
  const lightstyle = {
    backgroundColor: ' hsl(0, 0%, 98%)',
    color: '#000',
    marginTop: '5rem',
    width:'100vw',
    height:'91.4vh'
  }
  return (

    <div className="Main" style={!darktheme ? darkstyle :  lightstyle}>

      {isloading && !error && <Loader />}
      {/* {!isloading && error && <h4>{error}</h4>} */}
      <div className="button" >
        <button className={!darktheme? 'btn1 dark-theme ':'btn1 light-theme '} ><Link to='/' className={!darktheme?'cardbuttondark ah5':'cardbuttonlight ah5'}><i className="fa-solid fa-arrow-left" style={{ marginRight: '1rem' }}></i>Back</Link></button>
      </div>

      {country?.map((countrydata, index) => (
        <div className="countryinfocard" key={index}>
          <div className="cardimg">
            <img src={countrydata.flags.png} alt="" />
          </div>

          <div className="cardinfo">
            <h2 style={{marginBottom:'1rem'}}>{countrydata.name.common}</h2>


            <div className="cardinfodata">
              <div className="cardinfoleft">
                {/* <h5>
                  Native :
                  {countrydata.name}
                </h5> */}
                <h5>
                  Population:{" "}
                  <span>
                    {new Intl.NumberFormat().format(countrydata.population)}
                  </span>
                </h5>
                <h5>
                  Region: <span>{countrydata.region}</span>
                </h5>
                <h5>
                  Sub Region: <span>{countrydata.subregion}</span>
                </h5>
                <h5>
                  Capital: <span>{countrydata.capital}</span>
                </h5>
              </div>
              <div className="cardinforight">
                <h5>Top Level Domain :<span> {countrydata.tld} </span></h5>
                <h5>Currencies: {
                  countrydata.currencies ? (
                    <span>{Object.values(countrydata.currencies)[0].name}</span>
                  ) : (
                    <span>No currency information found</span>
                  )}
                </h5>
                <h5>Languages :


                  {Object.entries(countrydata.languages).map(([langCode, langName], index) => (
                    (
                      <span>{index > 0 && ','}  {langName}</span>
                    )
                  ))}

                </h5>
              </div>
            </div>
            <div className="borders">
              <h5>Border Countries :
                {
                  countrydata.borders && countrydata.borders.length > 0 ? (
                    countrydata.borders.map((border, index) => (
                      <span className={!darktheme ? 'borderspan dark-theme' : 'borderspan light-theme'}>
                        {border}

                      </span>

                    ))
                  ) : (
                    <span>No borders found</span>
                  )
                }
              </h5>

            </div>
          </div>
        </div>
      ))}



    </div>

  )
}

export default CountryInfo
