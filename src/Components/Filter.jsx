import React, { useState } from 'react'
import Subregion from './Subregion';
import { useHandle } from './GlobalContext/GlobalContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Filter = ({ onSearch, onfilter, handlepopulationFilter, populationFilter, handleareaFilter, areafilter, subregion }) => {
  const [input, setinput] = useState('');
  const {
    SelectedSubRegion,
    SetSelectedSubRegion,  darktheme
  } = useHandle()
  const [selectedRegion, setSelectedRegion] = useState('Filter by Region');
  const [region,setregion]=useState('');

  

  const submithandle = (e) => {
    e.preventDefault();

    onSearch(input);
  }
  const handleInputChange = (e) => {
    e.preventDefault()
    setinput(e.target.value.trim());
    onSearch(input)
    console.log(e.target.value.trim());
  }

  const handleRegionChange = (e) => {
    e.preventDefault()
    const region = e.target.value;
    setSelectedRegion(region);
    console.log(region);
    setregion(region);
    onfilter(region)

    SetSelectedSubRegion('')
    setinput('')
  };

  // useEffect(() => {

  //   const subregionObject = subregion();
  //   const initialSubregions = subregionObject[selectedRegion] || [];
  //   SetSelectedSubRegion(initialSubregions);
  // }, []);
  // console.log(1,region,"Subregion");
  
  const darkstyle = {
    backgroundColor: 'hsl(209, 23%, 22%)',
    color: 'hsl(0, 0%, 100%)'
}
const lightstyle = {
    backgroundColor: ' hsl(0, 0%, 98%)',
    color: '#000'
}



  return (
    <div className='filter'>
      <div  className={!darktheme ? 'dark-theme form' : ' form light-theme'}>

        <form onSubmit={submithandle}>
        
          <span type='submit'><i className="fa-solid fa-magnifying-glass"></i></span>
          <input type='text' value={input} placeholder='Search a country' onChange={handleInputChange} />
        </form>


      </div>
      <div className="filters">
        <select onChange={handlepopulationFilter} value={populationFilter} className={!darktheme ? 'dark-theme select1' : ' select1 light-theme'}>
          <option value='' disabled>Filter by Population</option>
          <option value='Increasing'>Increasing</option>
          <option value='Decreasing'>Decreasing</option>
        </select>
        <select  onChange={handleareaFilter} value={areafilter}className={!darktheme ? 'dark-theme select1' : ' select1 light-theme'}>
          <option value='' disabled>Filter by Area</option>
          <option value='Increasing'>Increasing</option>
          <option value='Decreasing'>Decreasing</option>
        </select>
        <Subregion subregion={subregion} region={region} style={!darktheme ? darkstyle : lightstyle} />
        
        <select value={selectedRegion} onChange={handleRegionChange} className={!darktheme ? ' select1 dark-theme' : 'light-theme select1'} >
          <option >Filter by Region</option>
          <option value='Africa'>Africa</option>
          <option value='Americas'>Americas</option>
          <option value='Asia'>Asia</option>
          <option value='Europe'>Europe</option>
        </select>
      </div>
    </div>
  )
}

export default Filter






































