import React from 'react'
import { useHandle } from './GlobalContext/GlobalContext';

const Subregion = ({ subregion, region }) => {

    const {

        SetSelectedSubRegion,
        SelectedSubRegion,
        darktheme

    } = useHandle()

    const handlesubregion = (e) => {
        SetSelectedSubRegion(e.target.value);
    }



    // const filteredSubregion = subregion[selectedRegion] || [];
    // console.log(filteredSubregion,'Filteredsubregion');
    // console.log(subregion,"Subregion component");
    // console.log(1,region,'subregion');


    return (
        

            <select onChange={handlesubregion}className={!darktheme ? 'dark-theme select2' : ' select2 light-theme'} >
            <option value="" disabled selected>Filter By SubRegion</option>
                {subregion && Object.entries(subregion).map(([regionselect, subRegion], index) => (
                    region === regionselect && (
                        subRegion.map((sub, subIndex) => (
                            <option key={subIndex} value={sub}>{sub}</option>
                        ))
                    )
                ))}
            </select>


     
    )
}

export default Subregion
