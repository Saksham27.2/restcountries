import React,{useContext, useState,createContext} from "react";

const GlobalContext=React.createContext()

export function useHandle()
{
    return useContext(GlobalContext)
}


export function GlobalProvider({children})
{
    const [countries, setcountries] = useState([]);
    const [filteredcountry, setfilteredcountry] = useState([]);
    const [error, setError] = useState("");
    const [isloading, setisloading] = useState(false)
    const [input, setinput] = useState('');
    const [selectedRegion, setSelectedRegion] = useState('');
    const [SelectedSubRegion, SetSelectedSubRegion] = useState('');
    const [darktheme,setdarktheme]=useState(true);
    const [populationFilter, setpopulationFilter] = useState('');
    const [areafilter, setareaFilter] = useState('');

    const  toggledarktheme=()=>{
        setdarktheme(prevtheme=>!prevtheme)
        console.log('Clicked');
    }
    const values={
        countries,
        setcountries,
        filteredcountry,
        setfilteredcountry,
        error,
        setError,
        isloading,
        setisloading,
        input,
        setinput,
        selectedRegion,
        setSelectedRegion,
        SelectedSubRegion,
        SetSelectedSubRegion,
        darktheme,
        setdarktheme,
        toggledarktheme,
        populationFilter,
        setpopulationFilter,
        areafilter,
        setareaFilter
    }
    

    return(
        <GlobalContext.Provider value={values} >
            {children}
        </GlobalContext.Provider>
    )
}