import './App.css'
import Allcountries from './Components/Allcountries';
import { useHandle } from '../src/Components/GlobalContext/GlobalContext'
import { Routes, Route } from 'react-router-dom'
import CountryInfo from './Components/CountryInfo';


function App() {
  const { darktheme, toggledarktheme } = useHandle();
  const darkstyle = {
    backgroundColor: 'hsl(209, 23%, 22%)',
    color: 'hsl(0, 0%, 100%)',
    background:'transparent'
    
  }
  const lightstyle = {
    backgroundColor: ' hsl(0, 0%, 98%)',
    color: '#000',
  }


  return (
    <>
      <div className={!darktheme ? 'header dark-theme' : 'header light-theme'}>
        <div className='container1'>

          <h1 style={!darktheme ? darkstyle : lightstyle}>Where in the world?</h1>
          <div className="darkmodebtn">

            <i className="fa-regular fa-moon"></i>
            <button onClick={toggledarktheme} className='btn' style={!darktheme ? darkstyle : lightstyle} >Dark Mode</button>
          </div>
        </div>

      </div>
      <div className="container">
        <Routes>
          <Route path='/' element={<Allcountries />} />
          <Route path='country/:countryName' element={<CountryInfo />} />
        </Routes>

      </div>
    </>
  )
}

export default App
